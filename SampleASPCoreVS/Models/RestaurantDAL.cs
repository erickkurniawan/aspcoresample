﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace SampleASPCoreVS.Models
{
    public class RestaurantDAL : IRestaurant
    {
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader dr;
        private IConfiguration _config;

        public RestaurantDAL(IConfiguration config)
        {
            _config = config;
        }

        private string GetConnStr()
        {
            return _config.GetConnectionString("DefaultConnection");
        }


        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public Restaurant Edit(string id, Restaurant obj)
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"update Restaurants set Name=@Name where ID=@ID";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Name", obj.Name);
                cmd.Parameters.AddWithValue("@ID", id);
                try
                {
                    conn.Open();
                    int result = cmd.ExecuteNonQuery();
                    if (result == 1)
                    {
                        obj.ID = Convert.ToInt32(id);
                    }
                    return obj;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }

        public IEnumerable<Restaurant> GetAll()
        {
            List<Restaurant> lstRestaurant = new List<Restaurant>();
            using(conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"select * from Restaurants order by Name asc";
                cmd = new SqlCommand(strSql, conn);
                conn.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lstRestaurant.Add(new Restaurant
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            Name = dr["Name"].ToString()
                        });
                    }
                }
                dr.Close();
                cmd.Dispose();
                conn.Close();
            }
            return lstRestaurant;
        }

        public Restaurant GetByID(string id)
        {
            Restaurant result = new Restaurant();
            using(conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"select * from Restaurants 
                                  where ID=@ID
                                  order by Name asc";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.AddWithValue("@ID", id);
                conn.Open();

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    result.ID = Convert.ToInt32(dr["ID"]);
                    result.Name = dr["Name"].ToString();
                }

                dr.Close();
                cmd.Dispose();
                conn.Close();
            }
            return result;
        }

        public IEnumerable<Restaurant> GetByName(string name)
        {
            List<Restaurant> lstRestaurant = new List<Restaurant>();
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = $"select * from Restaurants where Name like @Name order by Name asc";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.AddWithValue("@Name", "%"+name+"%");
                conn.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lstRestaurant.Add(new Restaurant
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            Name = dr["Name"].ToString()
                        });
                    }
                }
                dr.Close();
                cmd.Dispose();
                conn.Close();
            }
            return lstRestaurant;
        }

        public IEnumerable<Restaurant> GetByTahun(int tahun)
        {
            throw new NotImplementedException();
        }

        public Restaurant Insert(Restaurant obj)
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"insert into Restaurants(Name) values(@Name);select @@identity";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = obj.Name;
                try
                {
                    conn.Open();
                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                    obj.ID = id;
                    return obj;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }
    }
}
