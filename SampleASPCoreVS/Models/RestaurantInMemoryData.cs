﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreVS.Models
{
    public class RestaurantInMemoryData : IRestaurant
    {
        private List<Restaurant> _restaurant;
        public RestaurantInMemoryData()
        {
            _restaurant = new List<Restaurant>
            {
                new Restaurant{ID=1,Name="Bakmi Jawa Mbah Hadi"},
                new Restaurant{ID=2,Name="Sate Klathak Pak Jeje"},
                new Restaurant{ID=3,Name="Gudeg Djuminten"},
                new Restaurant{ID=4,Name="Tengkleng Gajah"}
            };
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public Restaurant Edit(string id, Restaurant obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Restaurant> GetAll()
        {
            //return _restaurant.OrderBy(r => r.Name);
            var results = from r in _restaurant
                          orderby r.Name descending
                          select r;

            return results;
        }

        public Restaurant GetByID(string id)
        {
            //var result = _restaurant.Where(r => r.ID.ToString() == id).SingleOrDefault();
            var result = (from r in _restaurant
                          where r.ID.ToString() == id
                          select r).SingleOrDefault();

            return result;
        }

        public IEnumerable<Restaurant> GetByName(string name)
        {
            /*List<Restaurant> listResult = new List<Restaurant>();
            foreach(var res in _restaurant)
            {
                if (res.Name.ToLower().Contains(name.ToLower()))
                {
                    listResult.Add(res);
                }
            }

            return listResult;*/
            var results = from r in _restaurant
                          where r.Name.ToLower().Contains(name.ToLower())
                          select r;

            return results;
        }

        public IEnumerable<Restaurant> GetByTahun(int tahun)
        {
            throw new NotImplementedException();
        }

        public Restaurant Insert(Restaurant obj)
        {
            throw new NotImplementedException();
        }
    }
}
