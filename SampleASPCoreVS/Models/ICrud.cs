﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreVS.Models
{
    public interface ICrud<T>
    {
        IEnumerable<T> GetAll();
        T GetByID(string id);
        T Insert(T obj);
        T Edit(string id,T obj);
        bool Delete(string id);
    }
}
