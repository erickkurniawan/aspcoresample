﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreVS.Models
{
    public interface IRestaurant : ICrud<Restaurant>
    {
        IEnumerable<Restaurant> GetByTahun(int tahun);
        IEnumerable<Restaurant> GetByName(string name);
    }
}
