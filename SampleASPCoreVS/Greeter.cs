﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreVS
{
    public class Greeter : IGreeter
    {
        public string GetMessage()
        {
            return "Ini pesan dari Greeter Class yang didaftarkan di DI";
        }
    }
}
