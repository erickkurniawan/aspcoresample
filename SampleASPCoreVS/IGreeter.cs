﻿namespace SampleASPCoreVS
{
    public interface IGreeter
    {
        string GetMessage();
    }
}