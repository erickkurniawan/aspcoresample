﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleASPCoreVS.Models;
using SampleASPCoreVS.ViewModels;

namespace SampleASPCoreVS.Controllers
{
    public class RestaurantController : Controller
    {
        private IRestaurant _restaurant;
        private IGreeter _greeter;
        public RestaurantController(IRestaurant restaurant,IGreeter greeter)
        {
            _restaurant = restaurant;
            _greeter = greeter;
        }
        public IActionResult Index()
        {
            var model = _restaurant.GetAll();
            ViewBag.Pesan = TempData["Pesan"];

            return View(model);
        }
        [HttpPost]
        public IActionResult Search(string search)
        {
            var model = _restaurant.GetByName(search);
            return View("Index",model);
        }
        public IActionResult Detail(string id)
        {
            var model = _restaurant.GetByID(id);
            return View(model);
        }

        public IActionResult IndexViewModel()
        {
            var model = new RestaurantViewModel();
            model.Restaurants = _restaurant.GetAll();
            model.Pesan = _greeter.GetMessage();

            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [ActionName("Create")]
        [HttpPost]
        public IActionResult CreatePost(Restaurant restaurant)
        {
            try
            {
                var result = _restaurant.Insert(restaurant);
                TempData["Pesan"] = "Data berhasil ditambah !";
                return RedirectToAction("Index");
                //ViewBag.Pesan = "Data Berhasil Ditambah !";
            }
            catch (Exception ex)
            {
                ViewBag.Pesan = "Error: " + ex.Message;
                return View();
            }
        }

        public IActionResult Edit(string id)
        {
            var result = _restaurant.GetByID(id);
            if (result != null)
            {
                return View(result);
            }
            return RedirectToAction("Index");
        }

        [ActionName("Edit")]
        [HttpPost]
        public IActionResult EditPost(string id,Restaurant restaurant)
        {
            //return Content(id.ToString() + " "+restaurant.Name);
            try
            {
                var result = _restaurant.Edit(id, restaurant);
                TempData["Pesan"] = "Data berhasil diedit !";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Pesan = "Data gagal diedit ! Error : "+ex.Message;
            }
            return View(restaurant);
        }
    }
}