﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleASPCoreVS.Models;

namespace SampleASPCoreVS.Controllers
{
    //custom route
    //[Route("company/[controller]/[action]")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = new Restaurant() {
                ID=2,
                Name = "Sate Klathak Pak Bari"
            };
            //return Content($"Id anda {id}, Nama {nama} dan Alamat {alamat}");
            ViewData["nama"] = "Erick";
            ViewBag.ListOfNama = new List<string> { "erick", "budi", "bambang", "joko", "ani" };

            return View(model);
        }

        public IActionResult About()
        {
            return Content("About Page");
        }

        public IActionResult Contact()
        {
            return Content("Contact Page");
        }
    }

}