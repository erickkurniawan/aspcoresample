﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreVS
{
    public class Pesan : IGreeter
    {
        private IConfiguration _config;
        public Pesan(IConfiguration config)
        {
            _config = config;
        }
        public string GetMessage()
        {
            return _config["Greeting"];
        }


    }
}
